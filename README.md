This is a program that simulates quantum computation. (A simulation is not the real thing, 
of course, and please do not expect to be able to run quantum processes on a classical 
computer via this or other simulation.)
It simulates one bit operations, such as identity, negation and Hadamard.
It also medels two bit operations, like CNOT (controlled not, this negates 
the second bit if the first is 1, otherwise not).
All operations follow the singleton pattern (lazy instantiation).
Operations are instantiated via a factory pattern. In order to avoid null pointers for 
the case the user asks for an invalid operation, I devised null operations 
(following the null object pattern). 
Therefore this program involves at least three design patterns.

See the entry point (Main) how to invoke operations and display their result.
